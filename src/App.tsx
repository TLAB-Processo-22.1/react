import React from 'react';
import './style/global.scss';
import { RepositoryList } from './components/RepositoryList';


function App() {
  return (
    <div>
      <RepositoryList/>
    </div>
  );
}

export default App;
